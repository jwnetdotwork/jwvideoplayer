# JW.ORG Video Player

## About
This JW.ORG Video Player is designed for people who want to play videos in JW.ORG using different subtitles and audio languages.  
For example, you can choose from Chinese subtitles and English audio.  

## Usage
Simply copy the video URL from JW.ORG and paste it into the URL box and select your favorite audio and subtitle languages.

## URL
[v.jw-net.work](https://v.jw-net.work/)

## License
MIT License