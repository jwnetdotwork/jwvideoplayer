import Plyr from 'plyr';
import toast from 'toastr';
import 'flowbite';

//css
import "./index.css";

const docdata = {};
let player;
const languages = {};
let current_lang = {};
let primary_lang = [];
let user_cookie = {};
let url_queries = {};
let metaUrl;
const lang = navigator.language
let scripts = [];
let current_script = [];
let isAutoPlay = true;
const animationspeed = 250;

function init() {
    user_cookie["last_video_lang"] = getCookie("last_video_lang");
    user_cookie["last_subtitles_lang"] = getCookie("last_subtitles_lang");
    user_cookie["last_used_langs"] = getCookie("last_used_langs").split(',');
    for (var i = 0; i < user_cookie["last_used_langs"].length; i++) {
        if (user_cookie["last_used_langs"][i] != "") {
            primary_lang.push(user_cookie["last_used_langs"][i]);
        }
    }
    loadAllLanguages();
    getQueries();
}

//select when focus on url field
$('#jwvideoplayer-input-url').on('focus', function () {
    $(this).select();
});


//submit
$('#jwvideoplayer-form').on("submit", function () {
    isAutoPlay = true;
    parseUrl($("#jwvideoplayer-input-url").val());
    return false;
})

//Start and Stop Ajax
$(document).ajaxStart(function () {
    // Show loading
    setLoadingBtn(true);
});
$(document).ajaxStop(function () {
    // Hide loading
    setLoadingBtn(false);
});


$(document).ajaxSend(function (event, jqXHR, ajaxOptions) {
    // console.log("ajaxSend:" + ajaxOptions.url);
}).ajaxComplete(function (event, jqXHR, ajaxOptions) {
    // console.log("ajaxComplete");
}).ajaxError(function (event, jqXHR, settings, exception) {
    if (jqXHR.status) {
        // console.log("ajaxError" + jqXHR.status);
    } else {
        // Error
        // console.log(jqXHR.status + ' ' + exception);
    }
});

$(window).on('popstate', function (e) {
    var state = e.originalEvent.state;
    if (!e.originalEvent.state) {
        return;
    }
    isAutoPlay = false;
    setPlayer(state.player_options, state.source);
    $("#jwvideoplayer-input-url").val(state.metaUrl);
    $('#jwvideoplayer-input-video-lang').val(state.vl);
    $('#jwvideoplayer-input-subtitles-lang').val(state.sl);
    $("#jwvideoplayer-title").text(state.player_options.title);
    document.title = state.player_options.title + ' - ' + 'JW.ORG Video Player';
});

function getQueries() {
    url_queries["url"] = getParameterByName('url');
    url_queries["vl"] = getParameterByName('vl');
    url_queries["sl"] = getParameterByName('sl');
    if (url_queries["url"] != null) {
        $("#jwvideoplayer-input-url").val(url_queries["url"]);
    }
}

function loadAllLanguages() {
    $.ajax({
        url: "https://b.jw-cdn.org/apis/mediator/v1/languages/E/all",
        type: "GET",
        data: { "clientType": "www" },
        dataType: "json",
        timeout: 10000,
        async: true,
    }).done(function (data1, textStatus, jqXHR) {
        if (data1["languages"][0]) {
            // console.log(data1["languages"].length + " languages. your lang: " + lang);
            languages["en"] = data1["languages"];
            var local_code = getCodefromlocal(lang)
            if (lang != "en" && local_code) {
                current_lang["locale"] = lang;
                current_lang["code"] = local_code;
                primary_lang.push(current_lang["code"]);
                $.ajax({
                    url: "https://b.jw-cdn.org/apis/mediator/v1/languages/" + current_lang["code"] + "/all",
                    type: "GET",
                    data: { "clientType": "www" },
                    dataType: "json",
                    timeout: 10000,
                }).done(function (data2, textStatus, jqXHR) {
                    // console.log("done loadMyLanguages:" + textStatus);
                    languages[current_lang["locale"]] = data2["languages"];
                    setLanguageOptions();
                }).fail((jqXHR, textStatus, errorThrown) => {
                    // console.log("fail loadMyLanguages:" + textStatus);
                    current_lang["code"] = "E";
                    current_lang["locale"] = "en";
                    primary_lang.push(current_lang["code"]);
                    setLanguageOptions();
                });
            } else {
                // console.log("goto setLanguageOptions");
                current_lang["code"] = "E";
                current_lang["locale"] = "en";
                primary_lang.push(current_lang["code"]);
                setLanguageOptions();
            }
        } else {
            // console.log("no languages");
            getFailBackLanguages();
        }
    }).fail((jqXHR, textStatus, errorThrown) => {
        // console.log("fail loadAllLanguages:" + textStatus);
        getFailBackLanguages();
    });
}

function getFailBackLanguages() {
    $.ajax({
        url: "./languages.json",
        type: "GET",
        data: {},
        dataType: "json",
        timeout: 10000,
    }).done(function (data2, textStatus, jqXHR) {
        // $("#log").append("done loadAllLanguages(alt):" + textStatus);
        if (data2["languages"][0]) {
            languages["en"] = data2["languages"];
            current_lang["code"] = "E";
            current_lang["locale"] = "en";
            primary_lang.push(current_lang["code"]);
            setLanguageOptions();
        } else {
            // console.log("loadAllLanguages(alt):No language data");
            toast.error("No Languages Available. Please contact to admin.");
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        // $("#log").append("fail loadAllLanguages(alt):" + textStatus);
        toast.error("Can not load to languages.json Please contact to admin.");
    });
}

function getCodefromlocal(locale) {
    const result = languages["en"].find((v) => v["locale"] == locale);
    return result ? result["code"] : undefined
}

function getlocalefromcode(code) {
    const result = languages["en"].find((v) => v["code"] == code);
    return result ? result["locale"] : undefined
}

function getnamefromcode(code) {
    var target_lang = languages[current_lang["locale"]] ? current_lang["locale"] : "en";
    const result = languages[target_lang].find((v) => v["code"] == code);
    return result ? result["name"] : undefined
}

function setLanguageOptions() {
    //console.log(primary_lang);
    var target_lang = languages[current_lang["locale"]] ? current_lang["locale"] : "en";
    var n = languages[target_lang].length;
    var primary_options_video = "", primary_options_subtitles = "", options_video = "", options_subtitles = "";
    var def_lang_video = url_queries["vl"] ? url_queries["vl"] : (user_cookie["last_video_lang"] ? user_cookie["last_video_lang"] : current_lang["code"]);
    var def_lang_subtitles = url_queries["sl"] ? url_queries["sl"] : (user_cookie["last_subtitles_lang"] ? user_cookie["last_subtitles_lang"] : current_lang["code"]);
    for (var i = 0; i < n; i++) {
        if (primary_lang.indexOf(languages[target_lang][i]["code"]) > -1) {
            primary_options_video += "<option value='" + languages[target_lang][i]["code"] + "' " + (languages[target_lang][i]["code"] == def_lang_video ? "selected" : "") + ">" + languages[target_lang][i]["name"] + "</option>";
            primary_options_subtitles += "<option value='" + languages[target_lang][i]["code"] + "' " + (languages[target_lang][i]["code"] == def_lang_subtitles ? "selected" : "") + ">" + languages[target_lang][i]["name"] + "</option>";

        } else {
            options_video += "<option value='" + languages[target_lang][i]["code"] + "' " + (languages[target_lang][i]["code"] == def_lang_video ? "selected" : "") + ">" + languages[target_lang][i]["name"] + "</option>";
            options_subtitles += "<option value='" + languages[target_lang][i]["code"] + "' " + (languages[target_lang][i]["code"] == def_lang_subtitles ? "selected" : "") + ">" + languages[target_lang][i]["name"] + "</option>";

        }
    }
    $('#jwvideoplayer-input-video-lang')[0].innerHTML = primary_options_video + options_video;
    $('#jwvideoplayer-input-subtitles-lang')[0].innerHTML = primary_options_subtitles + options_subtitles;
    if ($("#jwvideoplayer-input-url").val() != "") {
        isAutoPlay = false;
        parseUrl($("#jwvideoplayer-input-url").val());
    }
}

function parseUrl(url) {
    let re0 = /^https:\/\/(?:www|wol)\.jw\.org/i;
    let re1 = /mediaitems\/[^\/]+\/([^\/]+)/i;
    let re2 = /(?:item|lank)=([^&]+)/i;
    let re3 = /GETPUBMEDIALINKS/i;
    let re4 = /mediaitems\/([^\/]+)/i;
    let re5 = /docid=([^&]+)&data-video=webpubvid%3A%2F%2F%2F%3F(docid|pub)%3D([^%]+)%26track%3D([0-9]+)/i;//wol link
    let ary_docid;
    if ((ary_docid = re1.exec(url)) !== null || (ary_docid = re2.exec(url)) !== null || (ary_docid = re4.exec(url)) !== null) {
        metaUrl = url;
        executeDocId(ary_docid[1]);
    } else if (re3.test(url)) {
        metaUrl = url;
        executePub(url);
    } else if ((ary_docid = re5.exec(url)) !== null){
        metaUrl = "https://b.jw-cdn.org/apis/pub-media/GETPUBMEDIALINKS?output=json&" + ary_docid[2] + "=" + ary_docid[3] +"&fileformat=m4v%2Cmp4%2C3gp%2Cmp3&alllangs=1&track=" + ary_docid[4] + "&langwritten=J&txtCMSLang=J";
        executePub(metaUrl);
    } else {
        if (re0.exec(url) !== null) {
            $.ajax({
                url: "./api/getdocid.php",
                type: "POST",
                data: { url: url, },
                dataType: "json",
                timeout: 40000,
            }).done(function (output, statusText, jqXHR) {
                if (output.error) {
                    toast.error("Error: " + output.error);
                } else {
                    if (output.docid == "deffer") {
                        metaUrl = output.url;
                        executePub(output.url, output.poster);
                    } else if (output.docid != "error") {
                        executeDocId(output.docid);
                    } else {
                        toast.error("This url is not supported.");
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                toast.error("Can not connect to API. Please contact to admin.");
            }).always(() => {

            });
        } else {
            toast.error("This url is not supported.");
        }
    }
}

function setLoadingBtn(state) {
    if (state) {
        $('#jwvideoplayer-button-action svg').removeClass("hidden");
        $('#jwvideoplayer-button-action').prop("disabled", true);
        $('#jwvideoplayer-button-action').addClass("cursor-progress");
        $('#jwvideoplayer-button-action span').html('Loading...');
    } else {
        $('#jwvideoplayer-button-action svg').addClass("hidden");
        $('#jwvideoplayer-button-action').prop("disabled", false);
        $('#jwvideoplayer-button-action').removeClass("cursor-progress");
        $('#jwvideoplayer-button-action span').html('Open');
    }
}

function savelangselected() {
    setCookie("last_video_lang", $('#jwvideoplayer-input-video-lang').val(), 30);
    setCookie("last_subtitles_lang", $('#jwvideoplayer-input-subtitles-lang').val(), 30);

    pushLangHistory([docdata["video_lang"], docdata["subtitles_lang"]]);
}

function executeDocId(docid) {
    docdata["docid"] = docid;

    docdata["video_lang"] = $('#jwvideoplayer-input-video-lang').val();
    docdata["subtitles_lang"] = $('#jwvideoplayer-input-subtitles-lang').val();

    savelangselected();
    getmetaData();
}

function pushLangHistory(newLang) {
    for (var i = 0; i < newLang.length; i++) {
        if (user_cookie["last_used_langs"].indexOf(newLang[i]) > -1) {
            user_cookie["last_used_langs"].splice(user_cookie["last_used_langs"].indexOf(newLang[i]), 1);
        }
        user_cookie["last_used_langs"].unshift(newLang[i]);
        user_cookie["last_used_langs"] = user_cookie["last_used_langs"].slice(0, 10);
    }
    setCookie("last_used_langs", user_cookie["last_used_langs"].join(','), 30);
}

function getmetaData() {
    $.ajax({
        url: "https://b.jw-cdn.org/apis/mediator/v1/media-items/" + docdata["video_lang"] + "/" + docdata["docid"],
        type: "GET",
        data: { "clientType": "www" },
        dataType: "json",
        timeout: 10000,
    }).done(function (data1, textStatus, jqXHR) {
        // console.log("done LoadmetaData:" + textStatus);
        if (data1["media"][0]) {
            if (docdata["video_lang"] != docdata["subtitles_lang"]) {
                $.ajax({
                    url: "https://b.jw-cdn.org/apis/mediator/v1/media-items/" + docdata["subtitles_lang"] + "/" + docdata["docid"],
                    type: "GET",
                    data: { "clientType": "www" },
                    dataType: "json",
                    timeout: 10000,
                }).done(function (data2, textStatus, jqXHR) {
                    //console.log(textStatus);
                    if (data2["media"][0]) {// && data2["media"][0]["files"][0]["subtitles"]
                        //console.log();
                        setPlayerData(data1["media"][0], data2["media"][0]);
                    } else {
                        toast.error("The video not available in " + getnamefromcode(docdata["subtitles_lang"]));
                        setPlayerData(data1["media"][0], data1["media"][0]);
                    }
                });
            } else {
                setPlayerData(data1["media"][0], data1["media"][0]);
            }
        } else {
            toast.error("The video not available in " + getnamefromcode(docdata["video_lang"]));
        }

    }).fail((jqXHR, textStatus, errorThrown) => {
        console.log("fail LoadmetaData:" + textStatus);
        $.ajax({
            url: "./api/getmediaitems.php",
            type: "POST",
            data: {
                lang: docdata["video_lang"],
                docid: docdata["docid"],
            },
            dataType: "json",
            timeout: 10000,
        }).done(function (output, statusText, jqXHR) {
            // console.log("done LoadmetaData(Alt):" + statusText);
            if (output.error) {
                toast.error("Error: " + output.error);
            } else {
                if (docdata["video_lang"] != docdata["subtitles_lang"]) {
                    $.ajax({
                        url: "./api/getmediaitems.php",
                        type: "POST",
                        data: {
                            lang: docdata["subtitles_lang"],
                            docid: docdata["docid"],
                        },
                        dataType: "json",
                        timeout: 10000,
                    }).done(function (output2, statusText, jqXHR) {
                        // console.log("done LoadmetaData for subtitle:" + statusText);
                        if (output2.error) {
                            toast.error("Error: " + output2.error);
                        } else {
                            if (output2["media"][0]) {// && data2["media"][0]["files"][0]["subtitles"]
                                //console.log();
                                setPlayerData(output["media"][0], output2["media"][0]);
                            } else {
                                toast.error("The video not available in " + getnamefromcode(docdata["subtitles_lang"]));
                                setPlayerData(output["media"][0], output["media"][0]);
                            }
                        }
                        //alert(output.sum);
                        //alert(output.product);
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        // console.log("fail LoadmetaData for subtitle:" + textStatus);
                        toast.error("Can not connect to API. Please contact to admin.");
                    }).always(() => {

                    });
                } else {
                    setPlayerData(output["media"][0], output["media"][0]);
                }
            }
            //alert(output.sum);
            //alert(output.product);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            // console.log("fail LoadmetaData(alt):" + textStatus);
            toast.error("Can not connect to API. Please contact to admin.");
        }).always(() => {

        });
    });

    function setPlayerData(video, subtitle) {
        const player_options = {
            title: video["title"],
            captions: { active: true, language: getlocalefromcode(docdata["subtitles_lang"]), update: false },
            quality: { default: 480, options: [720, 480, 360, 240] },
            disableContextMenu: false,
        }

        var thumb_url = video["images"]["wss"] ? video["images"]["wss"]["lg"] : "";
        var source = {
            type: 'video',
            title: video["title"],
            sources: [],
            poster: thumb_url,
            tracks: [],
        }
        var videosizes = {
            "240p": "240",
            "360p": "360",
            "480p": "480",
            "720p": "720",
        }
        for (var i = 0; i < video["files"].length; i++) {
            if (videosizes[video["files"][i]["label"]]) {
                source["sources"].push({
                    src: video["files"][i]["progressiveDownloadURL"],
                    type: video["files"][i]["mimetype"],
                    size: videosizes[video["files"][i]["label"]],
                });
            }
        }
        var subtitles_files = [subtitle["files"], video["files"]];
        for (var j = 0; j < subtitles_files.length; j++) {
            for (var i = 0; i < subtitles_files[j].length; i++) {
                if (subtitles_files[j][i]["subtitles"]) {
                    source["tracks"].push({
                        kind: 'subtitles',
                        label: getnamefromcode(j == 0 ? docdata["subtitles_lang"] : docdata["video_lang"]),
                        srclang: getlocalefromcode(j == 0 ? docdata["subtitles_lang"] : docdata["video_lang"]),
                        src: subtitles_files[j][i]["subtitles"]["url"],
                        default: (j == 0 ? true : false),
                        title: (j == 0 ? subtitle["title"] : video["title"]),
                    });
                    break;
                }
            }
        }
        var addmessage = "";
        if (source["tracks"].length == 1 && source["tracks"][0]["default"] == false) {
            addmessage = getnamefromcode(docdata["video_lang"]) + " is used instead.";
        }
        if (video != subtitle && (source["tracks"].length == 0 || addmessage != "")) {
            toast.error("The subtitle not available in " + getnamefromcode(docdata["subtitles_lang"]) + addmessage);
        }

        setPlayer(player_options, source);
        setQueries(player_options, source);
    }
}

function executePub(url, poster = null) {
    savelangselected();

    var video_lang = $('#jwvideoplayer-input-video-lang').val();
    var subtitles_lang = $('#jwvideoplayer-input-subtitles-lang').val();

    const regex = /langwritten=[^&]+/i;
    var video_url = url.replace(regex, 'langwritten=' + video_lang);
    var subtitles_url = url.replace(regex, 'langwritten=' + subtitles_lang);

    $.ajax({
        url: video_url,
        type: "GET",
        dataType: "json",
        timeout: 10000,
    }).done(function (data1, textStatus, jqXHR) {
        //console.log(textStatus);
        if (data1["files"] && data1["files"][video_lang] && data1["files"][video_lang]["MP4"]) {
            if (video_lang != subtitles_lang) {
                $.ajax({
                    url: subtitles_url,
                    type: "GET",
                    dataType: "json",
                    timeout: 10000,
                }).done(function (data2, textStatus, jqXHR) {
                    //console.log(textStatus);
                    if (data2["files"] && data2["files"][subtitles_lang] && data2["files"][subtitles_lang]["MP4"]) {// && data2["media"][0]["files"][0]["subtitles"]
                        //console.log();
                        setPlayerMetaData(data1["files"][video_lang]["MP4"], data2["files"][subtitles_lang]["MP4"], poster);
                    } else {
                        toast.error("The video not available in " + getnamefromcode(subtitles_lang));
                        setPlayerMetaData(data1["files"][video_lang]["MP4"], data1["files"][video_lang]["MP4"], poster);
                    }

                });
            } else {
                setPlayerMetaData(data1["files"][video_lang]["MP4"], data1["files"][video_lang]["MP4"], poster);
            }
        } else {
            toast.error("The video not available in " + getnamefromcode(video_lang));
        }

    });

    function setPlayerMetaData(video, subtitle, poster = null) {
        const player_options = {
            title: video[0]["title"],
            captions: { active: true, language: getlocalefromcode(subtitles_lang), update: false },
            quality: { default: 480, options: [720, 480, 360, 240] },
            disableContextMenu: false,
        }

        var thumb_url = poster;
        var source = {
            type: 'video',
            title: video[0]["title"],
            sources: [],
            poster: thumb_url,
            tracks: [],
        }
        var videosizes = {
            "240p": "240",
            "360p": "360",
            "480p": "480",
            "720p": "720",
        }
        for (var i = 0; i < video.length; i++) {
            if (videosizes[video[i]["label"]]) {
                source["sources"].push({
                    src: video[i]["file"]["url"],
                    type: video[i]["mimetype"],
                    size: videosizes[video[i]["label"]],
                });
            }
        }
        var subtitles_files = [subtitle, video];
        for (var j = 0; j < subtitles_files.length; j++) {
            for (var i = 0; i < subtitles_files[j].length; i++) {
                if (subtitles_files[j][i]["subtitles"]) {
                    source["tracks"].push({
                        kind: 'subtitles',
                        label: getnamefromcode(j == 0 ? subtitles_lang : video_lang),
                        srclang: getlocalefromcode(j == 0 ? subtitles_lang : video_lang),
                        src: subtitles_files[j][i]["subtitles"]["url"],
                        default: (j == 0 ? true : false),
                        title: (j == 0 ? subtitle[0]["title"] : video[0]["title"]),
                    });
                    break;
                }
            }
        }

        var addmessage = "";
        if (source["tracks"].length == 1 && source["tracks"][0]["default"] == false) {
            addmessage = getnamefromcode(video_lang) + "is used instead.";
        }
        if (video != subtitle && (source["tracks"].length == 0 || addmessage != "")) {
            toast.error("The subtitle not available in " + getnamefromcode(subtitles_lang));
        }

        setPlayer(player_options, source);
        setQueries(player_options, source);
    }
}

function setPlayer(player_options, source) {
    if (player) {
        player.destroy();
    }
    scripts = [];
    current_script = [];
    player_options['autoplay'] = isAutoPlay;
    player = new Plyr('#player', player_options);
    player.source = source;

    player.on('timeupdate', playerOnTimeUpdate);
    player.on('play', function (event) {
        dataLayer.push({
            'event': 'video_start',
            'video_current_time': event.detail.plyr.currentTime,
            'video_duration': event.detail.plyr.duration,
            'video_quality': event.detail.plyr.quality,
        });
    });
    player.on('pause', function (event) {
        dataLayer.push({
            'event': 'video_pause',
            'video_current_time': event.detail.plyr.currentTime,
            'video_duration': event.detail.plyr.duration,
            'video_quality': event.detail.plyr.quality,
        });
    });

    $('#player').show();
    $("#jwvideoplayer-title").text(player_options["title"]);
    document.title = player_options["title"] + ' - JW.ORG Video Player';
    /* setup script */
    if (source["tracks"].length) {
        // var vtt_link = $("<div>Script: </div>");
        for (var i = 0; i < Math.max(source["tracks"].length, 2); i++) {
            if (source["tracks"][i]) {
                setupScript(source["tracks"][i], i);
                $("#scripttitle-" + i).text(source["tracks"][i]["title"]);
            }
        }
    }
    for (var i = source["tracks"].length; i < 2; i++) {
        $("#scriptblock-" + i).html("");
    }
    //$("#jwvideoplayer-script").html(vtt_link);
    $('#jwvideoplayer-header').show();
    dataLayer.push({
        'event': 'setPlayer',
        'video_lang': $('#jwvideoplayer-input-video-lang').val(),
        'subtitles_lang': $('#jwvideoplayer-input-subtitles-lang').val(),
        'doc_id': docdata["docid"] ? docdata["docid"] : $("#jwvideoplayer-input-url").val(),
    });
}

function setQueries(player_options, source) {
    var state_obj = {
        player_options: player_options,
        source: source,
        metaUrl: metaUrl,
        vl: $('#jwvideoplayer-input-video-lang').val(),
        sl: $('#jwvideoplayer-input-subtitles-lang').val(),
    }
    var url = "?url=" + encodeURIComponent(metaUrl) + "&vl=" + $('#jwvideoplayer-input-video-lang').val() + "&sl=" + $('#jwvideoplayer-input-subtitles-lang').val();
    history.pushState(state_obj, player_options["title"], url);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;secure; SameSite=Lax";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function setupScript(track, pos) {
    $.ajax({
        url: track['src'],
        type: "GET",
        dataType: "text",
        timeout: 10000,
    }).done(function (data1, textStatus, jqXHR) {
        let scriptLine = "";
        const splitted = data1.split(/\r\n\r\n|\n\n/);
        let re0 = /^(\d{2})?:?(\d{2}):(\d{2})\.(\d+) +--> +(\d{2})?:?(\d{2}):(\d{2})\.(\d+).*[\r\n]+\s*/mi;
        splitted.forEach(function (line, i) {
            const matches = line.match(re0)
            if (matches) {
                const stime = (matches[1] ? matches[1] : 0) * 3600 + matches[2] * 60 + Number(matches[3]) + matches[4] / 1000;
                const etime = (matches[5] ? matches[5] : 0) * 3600 + matches[6] * 60 + Number(matches[7]) + matches[8] / 1000;
                const intstime = parseInt(stime);
                const text = line.replace(matches[0], "");

                if (scripts[pos] == undefined) {
                    scripts[pos] = [];
                }
                if (scripts[pos][intstime] == undefined) {
                    scripts[pos][intstime] = [];
                }
                scripts[pos][intstime].push({
                    line: i,
                    stime: stime,
                    etime: etime,
                    text: text,
                })
                // scriptLine += "<div id='script-line-" + pos + "-" + i + "' data-pos='" + pos + "' data-ct='" + intstime + "'  data-idx='" + (scripts[pos][intstime].length - 1) + "' class='p-1 cursor-default'>" + text + "</div>";
            }

        })
        //in order to sort by time
        if (scripts[pos] != undefined) {
            scripts[pos].forEach(function (intstime, i) {
                intstime.forEach(function (line, j) {
                    scriptLine += "<div id='script-line-" + pos + "-" + line["line"] + "' data-pos='" + pos + "' data-ct='" + i + "'  data-idx='" + j + "' class='p-1 cursor-default'>" + line["text"] + "</div>";
                });
            });
        }
        $("#scriptblock-" + pos).html(scriptLine);
    });
}

function playerOnTimeUpdate(event) {
    const instance = event.detail.plyr;
    const cuurenttime = parseInt(instance.currentTime);
    for (let i = 0; i < scripts.length; i++) {
        for (let ct = cuurenttime - 1; ct <= cuurenttime; ct++) {
            if (ct > -1 && scripts[i][ct] != undefined) {
                for (let j = 0; j < scripts[i][ct].length; j++) {
                    if (scripts[i][ct][j]["stime"] <= instance.currentTime && scripts[i][ct][j]["etime"] >= instance.currentTime) {
                        if (current_script[i] == undefined || (current_script[i]["time"] != ct || current_script[i]["num"] != j)) {
                            if (current_script[i] != undefined) {
                                // console.log("remove:" + scripts[i][current_script[i]["time"]][current_script[i]["num"]]["text"]);
                                $("#script-line-" + i + "-" + scripts[i][current_script[i]["time"]][current_script[i]["num"]]["line"]).removeClass("bg-gray-100");
                            } else {

                            }
                            current_script[i] = { time: ct, num: j };
                            $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).addClass("bg-gray-100");
                            const position = $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).parent().scrollTop() + $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).position().top - $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).parent().height() / 8;
                            //console.log("top:" + $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).position().top + " position:" + position + " scrollTop:" + $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).parent().scrollTop() + " " + scripts[i][ct][j]["text"]);
                            $("#script-line-" + i + "-" + scripts[i][ct][j]["line"]).parent().animate({ scrollTop: position }, animationspeed);
                            // 
                        }

                    }
                }
            }
        }
    }
}

//Copy to Clipboard
$('.scriptcopybtn').on("click", function () {
    const target = document.getElementById($(this).data("target"))
    const range = document.createRange()
    const sel = window.getSelection()
    range.setStart(target.childNodes[0], 0)
    const endNode = target.childNodes[target.childNodes.length - 1]
    range.setEnd(endNode, endNode.childNodes.length)
    sel.removeAllRanges()
    sel.addRange(range)
    var result = document.execCommand('copy');
    //console.log(target + " " + result)
    return false;
})

//Seek video to the time of script which double clicked
$(".scriptblock").on("dblclick", "div", function (event) {
    // console.log("pos:" + $(this).data('pos') + " ct:" + $(this).data('ct') + " idx:" + $(this).data('idx'));
    const pos = $(this).data('pos'), ct = $(this).data('ct'), idx = $(this).data('idx');
    // console.log(scripts[pos][ct][idx]["stime"]);
    player.currentTime = scripts[pos][ct][idx]["stime"];
});

init();