module.exports = {
  content: ['./src/**/*.js', './dist/**/*.html', './dist/**/*.php', './node_modules/flowbite/**/*.js'],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
    },
  },
  plugins: [
    require('flowbite/plugin'),
  ],
}
