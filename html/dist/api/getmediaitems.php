<?php
    include __DIR__.'/functions.php';

    if(!isset($_POST['lang']) || !isset($_POST['docid'])){
        response(["error"=>'Language or Document id not set'], 400, true);
    }

    $lang = $_POST['lang'];
    $docid = $_POST['docid'];

    $url = "https://b.jw-cdn.org/apis/mediator/v1/media-items/" . $lang . "/" . $docid;
    $res = get_web_page($url);

    if ($res['err']) {
        response(["error"=>'sorry, an error occurred'], 500, true);
    } else {
        response($res['content'], 200, false);
    }
?>