<?php
    include __DIR__.'/functions.php';
    
    $data = ["docid"=>"deffer"];
    if(!isset($_POST['url'])){
        response(["error"=>'URL not set'], 400, true);
    }

    $url = $_POST['url'];
    $res = get_web_page($url);
    if ($res['err']) {
        response(["error"=>'sorry, an error occurred'], 500, true);
    } else {
        if(preg_match('/item=([^&]+)/i',$res['header']['url'],$matches)){
            $data['docid'] = $matches[1];
        }else{
            if(preg_match("/data-jsonurl\s*=\s*'([^']+)/i",$res['content'],$matches)){
                $data['url'] = str_replace("&alllangs=1","",$matches[1]);
            }else{
                $data['docid'] = "error";
            }

            if(preg_match('/class="jsVideoPoster\s[^"]+" data-src="([^"]+)/i',$res['content'],$matches)){
                $data['poster'] = $matches[1];
            }
        }
        response($data, 200, true);
    }

?>