<?php

    function response($res, $code=200, $isJson=true){
        http_response_code($code);
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: https://jw-net.work');
        if($isJson){
            $data = json_encode($res);
            if(json_last_error() == JSON_ERROR_NONE){
                echo $data;
            }
            else{
                http_response_code(500);
            }
        }else{
            echo $res;
        }        
        exit;
    }

    function get_web_page( $url ) {
        $res = array();
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     // return web page 
            CURLOPT_HEADER         => false,    // do not return headers 
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects 
            CURLOPT_USERAGENT      => "jw.org video player https://v.jw-net.work/", // who am i 
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect 
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect 
            CURLOPT_TIMEOUT        => 120,      // timeout on response 
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects 
        ); 
        $ch      = curl_init( $url ); 
        curl_setopt_array( $ch, $options ); 
        $res['content']  = curl_exec( $ch ); 
        $res['err']     = curl_errno( $ch ); 
        $res['errmsg']  = curl_error( $ch ); 
        $res['header']  = curl_getinfo( $ch ); 
        curl_close( $ch ); 
        return $res; 
    }  